const path = require('path')

const env = process.env.NODE_ENV;
const sourceMap = env === 'development';
console.log('SM?', sourceMap, env)

module.export = {
  mode: "development",
  entry: './src/app.js',
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: 'app.bundle.js'
  },
  cache: true,
  debug: true,
  devtool: 'eval-source-map',
  module: {
    rules: [
      {
        test: /\.js?$/,
        use: ["source-map-loader"],
        exclude: /node_modules/,
        loader: 'babel-loader',
        query: {
          presets: ['env']
        }
      }
    ]
  }
}